#! /bin/bash

#THIS SCRIPT LITERALLY NUKES ALL LOCAL DOCKER CONTAINERS, IMAGES AND VOLUMES!!!
#DON'T RUN IT UNLESS YOU REALLY WANT A TOTAL CLEAN SLATE WIPE
#AREN'T YOU GLAD YOU READ THIS BEOFRE YOU RAN IT? :)

#Stop on errors
set -e -x

#Test for admin permissions
#if [ "$(id -u)" -ne "0" ]
#then
#  echo "You must be root to run this script."
#  exit 1;
#fi

echo "Killing ALL docker containers!!!"
for i in $(docker ps -a | cut -c1-12 | grep -v "CONTAINER ID"); do docker kill ${i} | true; done

echo "Removing ALL docker containers!!!"
for i in $(docker ps -a | cut -c1-12 | grep -v "CONTAINER ID"); do docker rm -f ${i} | true; done

echo "Removing all docker volumes!!!"
for i in $(docker volume ls | cut -c21- | grep -v "VOLUME NAME"); do docker volume rm ${i} | true; done

echo "Nuking ALL docker images"
for i in $(docker image ls -q); do docker image rm $i --force; done

docker ps -a
docker volume ls
docker images
