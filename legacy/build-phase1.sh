#! /bin/sh
# Ready for quick code deployment

set -e # -e  Exit immediately if a command exits with a non-zero status.
set -x # -x  Print commands and their arguments as they are executed.

# Create image from dockerfile
docker build \
  --force-rm=true \
  -t audiogram-legacy \
  -f phase1-Dockerfile .
  
#Commands in Dockerfile run here!
