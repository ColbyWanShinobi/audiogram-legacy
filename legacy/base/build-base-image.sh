#! /bin/sh
# This script is responsible for creating the Base Docker image
# This consists of an up-to-date ubuntu with all dependencies installed
# Ready for quick code deployment

set -e # -e  Exit immediately if a command exits with a non-zero status.
set -x # -x  Print commands and their arguments as they are executed.

IMAGE=audiogram-base
DATE=$(date +'%Y%m%d')

# Create image from dockerfile
docker build \
  --force-rm=true \
  -t ${IMAGE}:${DATE} \
  -f Dockerfile .
  
#Commands in Dockerfile run here!

docker tag ${IMAGE}:${DATE} $IMAGE:latest
docker save -o ${IMAGE}-${DATE}.tar $IMAGE
docker save -o ${IMAGE}-latest.tar $IMAGE
gzip -f ${IMAGE}-${DATE}.tar
gzip -f ${IMAGE}-latest.tar
